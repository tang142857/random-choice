"""
User inter face of random choice

Now random choice(ranc) use tkinter as gui package
In the future ,may change to the Qt(pyside)

@author: Tang142857
@project: random-choice
@file: gui.py
@date: 2021-10-06
Copyright(c): DFSA Software Develop Center
"""
import tkinter
import tkinter.font as tk_font

import icon


class RCWidgets(object):
    def __init__(self, root: tkinter.Tk):
        icon_name = icon.make_icon()
        self.win = root
        icon_file = tkinter.PhotoImage(file=icon_name)
        self.win.tk.call('wm', 'iconphoto', self.win._w, icon_file)
        icon.clean_icon(icon_name)
        # XXX set the icon
        self.root_win = tkinter.Frame(self.win, background='black')
        # XXX to create the color of the window ,the root frame name
        # as 'root_win' !!!!

        self.push_button_font = tk_font.Font(self.win, family='隶书', size=18)
        self.name_show_font = tk_font.Font(self.win, family='宋体', size=72)

    def setup(self):
        """
        Setup the ui widgets
        """
        self.win.title('课堂提问随机产生器')
        self.win.geometry('682x231')
        self.win.resizable(False, False)
        # init the widgets
        self.display = tkinter.Label(self.root_win,
                                     text='SOME ONE',
                                     height=86,
                                     width=401,
                                     background='black',
                                     foreground='white',
                                     font=self.name_show_font,
                                     borderwidth=3,
                                     relief=tkinter.RIDGE,
                                     highlightcolor='white')
        self.button_frame = tkinter.Frame(self.root_win, background='green')
        self.start_button = tkinter.Button(self.button_frame,
                                           text='下一个',
                                           font=self.push_button_font,
                                           background='#ffe0c0')
        self.reset_button = tkinter.Button(self.button_frame,
                                           text='重置',
                                           font=self.push_button_font,
                                           background='#ffc0ff')
        # Just to look like the origin version ,normally no use
        # pack the widgets
        self.root_win.place(x=0, y=0, height=231, width=682)
        self.display.place(width=401, height=113, x=48, y=48)
        self.button_frame.place(x=470, y=62, width=170, height=78)
        self.start_button.place(x=0, y=0, height=78, width=103)
        self.reset_button.place(x=103, y=0, height=78, width=67)

    def set_name(self, name):
        self.display.config(text=name)


if __name__ == '__main__':
    test_root = tkinter.Tk()
    WD = RCWidgets(test_root)
    WD.setup()
    WD.set_name('LAOLANG')
    test_root.mainloop()
