"""
The data file of random choice
You may need to change the file ,
or directly override the get_data function

@author: Tang142857
@project: random-choice
@file: data.py
@date: 2021-10-05
Copyright(c): DFSA Software Develop Center
"""
DATA = [
    '安芮滋',
    '蔡若滢',
    '陈龚',
    '程一桓',
    '冯睿莹',
    '龚熙杰',
    '何睿涵',
    '何育恒',
    '胡馨月',
    '华观中',
    '黄瑞陈',
    '蒋云天',
    '康睿涵',
    '雷钰桐',
    '冷逸涵',
    '李承罡',
    '李雄韬',
    '廖郎',
    '刘栋瑞',
    '刘素汶',
    '刘亚鹏',
    '罗海迅',
    '吕玥仪',
    '彭霖宇',
    '舒天昊',
    '宋明哲',
    '孙豪',
    '汤达',
    '唐巳珩',
    '王皓可',
    '王可',
    '吴思睿',
    '吴彦臻',
    '吴燕芸',
    '吴悠',
    '吴宇',
    '熊迪文',
    '熊海潼',
    '杨浩宇',
    '杨嘉成',
    '杨文皓',
    '杨颜嘉',
    '于海翼',
    '于泽镐',
    '袁睿婷',
    '张健翔',
    '张桐铭',
    '章天祎',
    '周文哲',
    '周奕君',
    '黄山小',
    '马山',
]


def get_date() -> list:
    """
    Return the data.
    :return: data list
    """
    # TODO improve the data store safety
    return DATA