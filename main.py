"""
Main and run entry of random choice

@author: Tang142857
@project: random-choice
@file: main.py
@date: 2021-10-06
Copyright(c): DFSA Software Develop Center
"""
import copy
import random
import sys
import threading
import time
import tkinter

import data
import gui


class Choice(object):
    def __init__(self, data: list):
        """
        Choose a random element in the list
        """
        self._data = data
        self._process_data = data * 10
        self._data_copy = copy.deepcopy(data)

    def _get_next(self):
        """
        The main function ,
        You may need to override the function
        :return: self.data's item
        """
        process_dogs = random.sample(self._process_data, 30)
        dog = random.sample(self._data, 1)[0]
        if len(self._data) == 1:  # the last dog
            self._data = copy.deepcopy(self._data_copy)
        else:
            self._data.remove(dog)
        # let everyone can be choosed

        sys.stdout.write(
            f'The processed dogs {process_dogs}\nThe final dog {dog}\n')
        sys.stdout.write(f'The wait-for-choosing dog {self._data}\n\n')

        def process():
            # use the function to make a flash name effect
            for p_dog in process_dogs:
                WIDGETS.set_name(p_dog)
                time.sleep(0.03)
            WIDGETS.set_name(dog)

        threading.Thread(target=process).start()

    def handler(self):
        """Button call this function to work"""
        self._get_next()
        # WIDGETS.set_name(next_dog)


if __name__ == '__main__':
    sys.stdout.write('The debug console of RANC\n')
    MAIN_WINDOW = tkinter.Tk()
    WIDGETS = gui.RCWidgets(MAIN_WINDOW)
    WIDGETS.setup()
    # Load the data base
    ZOO = Choice(data=data.get_date())
    # bind the buttons ,XXX the reset has no callback :)
    WIDGETS.start_button.config(command=ZOO.handler)
    # Start loop
    MAIN_WINDOW.mainloop()
